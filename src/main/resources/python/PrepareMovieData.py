import json,uuid, names,random
#https://github.com/FEND16/movie-json-data

with open("moviedata.json","r") as f:
	data = json.load(f)
	
template = """INSERT INTO VIDEO_ITEM  (ID,TITLE,SUB_TITLE ,IMAGE_SRC ,VIDEO_SRC  )
VALUES ('#id#','#title#','#sub#','#img#','TearsOfSteel.mp4');

INSERT INTO VIDEO_DESCRIPTION (ID,DESCRIPTION )
VALUES ('#id#','#DESCRIPTION#');

"""

template2 = """INSERT INTO USER_INFO (ID,USER_NAME)
VALUES ('#id#','#username#');

"""

movieIds = []
userIds = []
usernames = []

with open("movies.sql","w") as f:
	for movie in data:
		id = str(uuid.uuid4())
		movieIds.append(id);
		title = movie['originalTitle'] if movie['originalTitle'] != "" else movie['title']
		title = title.replace("'","''")
		subt = movie["year"]
		img = movie['poster']
		desc = movie["storyline"].replace("\n"," ").replace("'","''")
		desc += " tags: "
		for genre in movie['genres']:
			desc += genre.replace("'","''") + " "	
		for actor in movie['actors']:
			desc += actor.replace("'","''") + " " #"""
			
		insert = template.replace("#id#",id);
		insert = insert.replace("#title#",title)
		insert = insert.replace("#sub#",subt)
		insert = insert.replace("#img#",img)
		insert = insert.replace("#DESCRIPTION#",desc)
		
		f.write(insert);

template = """INSERT INTO USER_INFO (ID,USER_NAME)
VALUES ('#id#','#username#');

"""
		
with open("users.sql","w") as f:
	for _ in range(10000):
		id = str(uuid.uuid4())
		name = names.get_full_name().replace(" ","")
		if name in usernames:
			continue
		usernames.append(name);
		userIds.append(id)
		insert = template.replace("#id#",id);
		insert = insert.replace("#username#",name)
		f.write(insert);
		

template = """INSERT INTO META_DATA (ID,MOVIE_ID,USER_ID,PERCENTAGE_WATCHED)
VALUES ('#id#','#mid#','#uid#','#pw#');

"""

pws = []
pws.extend(range(1,20))
pws.extend(range(80,101))

with open("metadata.sql","w") as f:
	for _ in range(100000):
		id = str(uuid.uuid4())
		userId = random.choice(userIds)
		movieId = random.choice(movieIds)
		pw = random.choice(pws)
		
		insert = template.replace("#id#",id);
		insert = insert.replace("#uid#",userId)
		insert = insert.replace("#mid#",movieId)
		insert = insert.replace("#pw#",str(pw))
		f.write(insert);
	
	